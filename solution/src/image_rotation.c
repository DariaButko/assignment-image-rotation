#include "image_rotation.h"
#include <stddef.h>

struct image rotate(const struct image source) {
    struct image new = image_construct(source.height, source.width);

    for (size_t i = 0; i < source.height; ++i) {
        for (size_t j = 0; j < source.width; ++j) {
            image_set_pixel(new, *image_get_pixel(source, j, source.height - i - 1), i, j);
        }
    }
    return new;
}
