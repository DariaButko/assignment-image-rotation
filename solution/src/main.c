#include "image.h"
#include "bmp_extension.h"
#include "image_rotation.h"
#include <stdio.h>


#define ERROR_EXIT_CODE 1

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Invalid amount of arguments: expected 2, got %d", argc - 1);
        return ERROR_EXIT_CODE;
    }

    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");
    if (in == NULL || out == NULL) {
        fprintf(stderr, "Error opening files at paths %s, %s", argv[1], argv[2]);
        return ERROR_EXIT_CODE;
    }

    struct image image = {0};

    if (from_bmp(in, &image) != READ_OK) {
        fprintf(stderr, "Failed to read bmp image from %s", argv[1]);
        return ERROR_EXIT_CODE;
    }

    struct image rotated = rotate(image);

    if (to_bmp(out, &rotated) != WRITE_OK) {
        fprintf(stderr, "Error writing image to %s", argv[2]);
        image_free(image);
        image_free(rotated);
        return ERROR_EXIT_CODE;
    }

    image_free(image);
    image_free(rotated);
    return 0;
}
