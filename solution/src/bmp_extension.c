#include "bmp_extension.h"
#include <stdbool.h>

#define SUCCESS 0
#define BM 0x4d42 // 'B' and 'M' ascii codes
#define DEPTH 24

struct __attribute__((packed)) bmp_header {
    uint16_t  bfType;
    uint32_t  bfileSize;
    uint32_t  bfReserved;
    uint32_t  bOffBits;
    uint32_t  biSize;
    uint32_t  biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t  biBitCount;
    uint32_t  biCompression;
    uint32_t  biSizeImage;
    uint32_t  biXPelsPerMeter;
    uint32_t  biYPelsPerMeter;
    uint32_t  biClrUsed;
    uint32_t  biClrImportant;
};

static size_t image_get_padding(const struct image image) {
    return image.width % 4;
}

static bool read_pixels(FILE* in, struct image img){
    const size_t padding = image_get_padding(img);

    for (size_t i = 0; i < img.height; ++i) {
        if (fread(img.data + img.width * i, sizeof(struct pixel), img.width, in) != img.width) { // read one row of pixels
            return false;
        }

        if (fseek(in, (long)padding, SEEK_CUR) != SUCCESS) { // skip padding
            return false;
        }
    }

    return true;
}

static bool header_correct_type(const struct bmp_header header) {
    return header.bfType == BM;
}

static bool header_correct_depth(const struct bmp_header header) {
    return header.biBitCount == DEPTH;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (in == NULL || img == NULL) {
        return READ_INVALID_INPUT;
    }

    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) { // 1 header struct should be read
        return READ_INVALID_HEADER;
    }

    if (!header_correct_type(header)) {
        return READ_INVALID_SIGNATURE;
    }

    if (!header_correct_depth(header)){
        return READ_INVALID_HEADER;
    }

    if (fseek(in, (long)header.bOffBits, SEEK_SET) != SUCCESS){
        return READ_INVALID_BITS;
    }

    struct image image_placeholder = image_construct(header.biWidth, header.biHeight);

    // not reading into external memory straight away
    if (read_pixels(in, image_placeholder) == false){
        image_free(image_placeholder);
        return READ_INVALID_BITS;
    }

    *img = image_placeholder;

    return READ_OK;
}

static struct bmp_header create_header(const struct image* img) {
    const size_t padding = image_get_padding(*img);

    const uint32_t RESERVED = 0;
    const uint32_t BISIZE = 40;
    const uint32_t PLANES = 1;
    const uint32_t COMPRESSION = 0;
    const uint32_t PPM = 2834;
    const uint32_t C_USED = 0;
    const uint32_t C_IMP = 0;

    struct bmp_header header = {0};
    header.bfType = BM;
    header.bfileSize = sizeof(struct bmp_header) + img->width * img->height * sizeof(struct pixel) + (img->height + 1) * padding;
    header.bfReserved = RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BISIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = PLANES;
    header.biBitCount = DEPTH;
    header.biCompression = COMPRESSION;
    header.biSizeImage = img->width * img->height * sizeof(struct pixel) + (img->height + 1) * padding;
    header.biXPelsPerMeter = PPM;
    header.biYPelsPerMeter = PPM;
    header.biClrUsed = C_USED;
    header.biClrImportant = C_IMP;

    return header;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    if (out == NULL || img == NULL) {
        return WRITE_INVALID_INPUT;
    }

    const struct bmp_header header = create_header(img);
    const size_t padding = image_get_padding(*img);
    const uint8_t padding_placeholder[3] = {0};


    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) { // 1 header struct should be written
        return WRITE_HEADER_ERROR;
    }

    for (size_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out) !=
            img->width) { // width bytes should be written
            return WRITE_BITS_ERROR;
        }

        if (fwrite(padding_placeholder, sizeof(uint8_t), padding, out) != padding) { //padding bytes should be written
            return WRITE_PADDING_ERROR;
        }
    }
    return WRITE_OK;
}
