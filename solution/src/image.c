#include "image.h"

#include <malloc.h>
#include <stdbool.h>

struct image image_construct(uint32_t width, uint32_t height) {
    struct pixel* data = malloc(width * height * sizeof(struct pixel));
    return (struct image) {
        .width = width,
        .height = height,
        .data = data
    };
}

void image_free(struct image image) {
    free(image.data);
}

static bool invalid_position(const struct image image, uint32_t x, uint32_t y) {
    return x >= image.width || y >= image.height;
}

static struct pixel* image_pixel_at(const struct image image, uint32_t x, uint32_t y) {
    return image.data + x + (image.width * y);
}

struct pixel* image_get_pixel(const struct image image, uint32_t x, uint32_t y) {
    if (invalid_position(image, x, y)) {
        return NULL;
    }

    return image_pixel_at(image, x, y);

}

void image_set_pixel(struct image image, struct pixel pixel, uint32_t x, uint32_t y) {
    if (invalid_position(image, x, y)) {
        return;
    }

    *image_pixel_at(image, x, y) = pixel;
}
