#ifndef IMAGE_ROTATION_H
#define IMAGE_ROTATION_H
#include "image.h"

struct image rotate(const struct image source);

#endif
