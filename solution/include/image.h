#ifndef IMAGE_H
#define IMAGE_H
#include <inttypes.h>

struct pixel { 
    uint8_t b, g, r; 
};

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image image_construct(uint32_t width, uint32_t height);

void image_free(struct image image);

struct pixel* image_get_pixel(const struct image image, uint32_t x, uint32_t y);

void image_set_pixel(struct image image, struct pixel pixel, uint32_t x, uint32_t y);

#endif
