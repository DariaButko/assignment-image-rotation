#ifndef BMP_EXTENSION_H
#define BMP_EXTENSION_H

#include "image.h"
#include <stdio.h>

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_INPUT,
  READ_INVALID_DEPTH
};

enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
  WRITE_OK = 0,
  WRITE_HEADER_ERROR,
  WRITE_PADDING_ERROR,
  WRITE_BITS_ERROR,
  WRITE_ERROR,
  WRITE_INVALID_INPUT
};

enum write_status to_bmp( FILE* out, struct image const* img );
#endif
